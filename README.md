# Album Download Page for Luke and the Charmers Transition
## Setup
- run npm install
- after changes to "unencrypted.html" run "npx pagecrypt unencrypted.html index.html mySuperSecurePassword123" 
- add the css from custom-encrypt-css.css to index.html
- make desired changes to the index.html (remove branding, change wording)
- upload index.html